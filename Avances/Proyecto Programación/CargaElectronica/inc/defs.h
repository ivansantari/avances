/*
 * defs.h
 *
 *  Created on: Aug 8, 2020
 *      Author: Fabi
 */

#ifndef DEFS_H_
#define DEFS_H_

// Valor máximo de la rampa (<1024)
#define MAX_VALUE 1000

// Tick para actualizar el valor de la rampa
#define TIMER_TICK_RATE_HZ 1000

// Paso de actualización de la rampa
#define STEP 1

/* Definiendo una rampa con valor máximo de 1000,
 * con paso de 1 y que se actualice cada 1ms,
 * nos da una rampa de período 1 seg (1 Hz)
 */

// Valor de barrera para encender el led
#define THRESHOLD 500



#endif /* DEFS_H_ */
