#include "LCD_CHAR.h"
#include "chip.h"
#include <cr_section_macros.h>
#include "FreeRTOS.h"

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif


#define delay_short	10000 //250
#define delay_long	500000 //5000

/*-------------------------------------------------------------------*/
void delay(unsigned int dl){
	unsigned int d;
	for(d=0;d<dl;d++);
}
/*-------------------------------------------------------------------*/
void nibble_lcd4(unsigned char nb){
	nb &= 0x0F;
	if((nb>>3 & 1)==1){
		Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_D,(1<<DB7));
	}else{Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_D,(1<<DB7));}
	if((nb>>2 & 1)==1){
			Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_D,(1<<DB6));
		}else{Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_D,(1<<DB6));}
	if((nb>>1 & 1)==1){
			Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_D,(1<<DB5));
		}else{Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_D,(1<<DB5));}
	if((nb & 1)==1){
			Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_D,(1<<DB4));
		}else{Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_D,(1<<DB4));}
	/*DB7 = (nb>>3 & 1);
	DB6 = (nb>>2 & 1);
	DB5 = (nb>>1 & 1);
	DB4 = (nb & 1);*/
}
/*-------------------------------------------------------------------*/
void write_com_lcd4(unsigned char wb){
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<RS));//RS = 0;
	//RW = 0;
	nibble_lcd4(wb >> 4);		//data h
	Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 1;
	delay(delay_short);
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 0;
	nibble_lcd4(wb);			//data l
	Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 1;
	delay(delay_short);
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 0;
	delay(delay_short);
}
/*-------------------------------------------------------------------*/
void write_data_lcd4(unsigned char wd){
	Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_R,(1<<RS));//RS = 1;//RW = 0;
	nibble_lcd4(wd >> 4);		// data h
	Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 1;
	delay(delay_short);
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 0;
	nibble_lcd4(wd);			// data l
	Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 1;
	delay(delay_short);
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 0;
	delay(delay_short);
}
/*-------------------------------------------------------------------*/
void mode4bit(void){
	//delay(delay_long);
	delay(delay_long);
	nibble_lcd4(3);
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<RS));//RS = 0;
	Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 1;
	delay(delay_short);
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 0;
	delay(delay_short);
	nibble_lcd4(3);
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<RS));//RS = 0;
	Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 1;
	delay(delay_short);
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 0;
	delay(delay_short);
	nibble_lcd4(2);
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<RS));//RS = 0;
	Chip_GPIO_SetValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 1;
	delay(delay_short);
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 0;
	delay(delay_short);
}
/*-------------------------------------------------------------------*/
void ClearDisplay4(void){
	write_com_lcd4(0x01);		// Display clear
//	while(check_busy4()==BUSY);
	delay(delay_long);			// delay 1.53mseg
	delay(delay_long);
	delay(delay_long);
}
/*-------------------------------------------------------------------*/
void ReturnHome4(void){
	write_com_lcd4(0x02);		// Display clear
//	while(check_busy4()==BUSY);
	delay(delay_long);			// delay 1.53mseg
}
/*-------------------------------------------------------------------*/
void EntryModeSet4(unsigned char ems){
	ems &= 0x03; ems |= 0x04;
	write_com_lcd4(ems);		// Entry mode set: I/D=1-increment mode,SH=0-shift off
//	while(check_busy4()==BUSY);
	delay(delay_short);			// delay 39useg
}
/*-------------------------------------------------------------------*/
void DisplayOnOff4(unsigned char dof){
	dof &= 0x07; dof |= 0x08;
	write_com_lcd4(dof);		// Display ON/OFF:D=1 display on,C=0 cursor off,B=0,blink off
//	while(check_busy4()==BUSY);
	delay(delay_short);			// delay 39useg
}
/*-------------------------------------------------------------------*/
void CurDisShift4(unsigned char cdf){
	cdf &= 0x0C; cdf |= 0x10;
	write_com_lcd4(cdf);		// cursor display shift
//	while(check_busy4()==BUSY);
	delay(delay_short);			// delay 39useg
}
/*-------------------------------------------------------------------*/
void FuntionSet4(unsigned char fs){
	fs &= 0x1C; fs |= 0x20;
	write_com_lcd4(fs);		// Funtion set: 8bit bus,N=1-1line,F=0-5x8dots
//	while(check_busy4()==BUSY);
	delay(delay_short);			// delay 39useg
}
/*-------------------------------------------------------------------*/
void SetCGRAMAddress4(unsigned char sca){
	sca &= 0x3F; sca |= 0x40;
	write_com_lcd4(sca);		// Funtion set: 8bit bus,N=1-1line,F=0-5x8dots
//	while(check_busy4()==BUSY);
	delay(delay_short);			// delay 39useg
}
/*-------------------------------------------------------------------*/
void SetDDRAMAddress4(unsigned char sda){
	sda &= 0x7F; sda |= 0x80;
	write_com_lcd4(sda);		// Funtion set: 8bit bus,N=1-1line,F=0-5x8dots
//	while(check_busy4()==BUSY);
	delay(delay_short);			// delay 39useg
}
/*-------------------------------------------------------------------*/
void lcd_char_init4(void){
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<RS));
	Chip_GPIO_ClearValue(LPC_GPIO,puerto_lcd_R,(1<<E1));//E1 = 0;//RS=0;E1=0;
	mode4bit();
//	FuntionSet4(0x28);
//	DisplayOnOff4(0x08);
//	ClearDisplay4();
//	EntryModeSet4(0x06);
//	ClearDisplay4();
//	ReturnHome4();

	FuntionSet4(0x28);
	DisplayOnOff4(0x0C);
//	ClearDisplay4();
	EntryModeSet4(0x06);
//	ClearDisplay4();
//	ReturnHome4();
}
/*-------------------------------------------------------------------*/
/*-------------------------------------------------------------------*/
void line_lcd4(unsigned char *p,unsigned char r){
	if(r==1) r = 0;
	if(r==2) r = 0x40;
	if(r==3) r = 0x14;
	if(r==4) r = 0x54;

	SetDDRAMAddress4(r);
	r = 0;
	while(r<20){
		write_data_lcd4(*p);
		p++;
		r++;
		if(*p == 0) r = 20;
	}
}
/*-------------------------------------------------------------------*/
unsigned char char_read_lcd4(void){
	unsigned char l;
	l = read_data_lcd4();
	return l;
}
/*-------------------------------------------------------------------*/

