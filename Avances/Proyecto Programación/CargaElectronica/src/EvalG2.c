

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

/*
 *Headers utilizados
 */
#include <cr_section_macros.h>
#include "math.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "string.h"
#include "LCD_CHAR.h"
#include "i2c.h"

/*
 * Definición de los prototipos
 * */
void initlcd(void);
void initencoder(void);
void initi2c(void);

/*Estructura utilizada por la cola*/
struct dat{
	unsigned char corriente[16];
};

/* Definición de la cola*/
xQueueHandle xQueue = NULL;

#define encoderSW	 	13
#define encoderA	 	28
#define encoderB	 	27
#define SLAVE_ADDRESS	0x40

static void Principal(void *pvParameters){
	volatile unsigned char menu1=0, auxmenu1=0, stateSW, stateA, stateB, linea2[16], posicion=1, i;
	struct dat dat1;
	for(i=0;i<16;i++)
	{
		if (i<7){dat1.corriente[i]=48;}
		if (i==3){dat1.corriente[i]=46;}
		if (i==7){dat1.corriente[i]=65;}
		if (i>7 || i==0){dat1.corriente[i]=32;}
		if (i==1){linea2[i]=42;}
		if (i>1 && i<7){linea2[i]=45;}
		if (i>6 || i==0){linea2[i]=32;}

	}
	while(1)
	{
		stateSW = Chip_GPIO_ReadPortBit(LPC_GPIO,2,encoderSW);
		stateA = Chip_GPIO_ReadPortBit(LPC_GPIO,0,encoderA);
		stateB = Chip_GPIO_ReadPortBit(LPC_GPIO,0,encoderB);
		if(stateSW == 0)
		{
			vTaskDelay(300/portTICK_RATE_MS);
			if(menu1 == 0 && auxmenu1 == 0)
			{
			menu1=1;
			ClearDisplay4();
			line_lcd4(dat1.corriente,1);
			line_lcd4(linea2,2);
			}
			if(menu1 == 1 && auxmenu1 == 2)
			{
			menu1=2;
			}
			if (menu1 == 1 && auxmenu1 == 1)
			{
				if(posicion < 7)
				{
					if(posicion != 3)
					{
						linea2[posicion]=45;
						if(posicion != 6){
						posicion++;
						linea2[posicion]=42;}
						else posicion++;
					}
					if(posicion == 3)
					{
						linea2[posicion]=45;
						posicion++;
						linea2[posicion]=42;
						line_lcd4(linea2,2);
					}
					line_lcd4(linea2,2);
				}
				if(posicion == 7)
				{
					posicion = 1;
					linea2[posicion]=42;
					line_lcd4(linea2,2);
				}
			}
			auxmenu1 = 1;
		}
		if(stateA == 0 && menu1 == 1)
		{
			vTaskDelay(100/portTICK_RATE_MS);
			if(dat1.corriente[posicion] > 57) {dat1.corriente[posicion] = 57;}
			if(dat1.corriente[posicion] < 57)
			{
				dat1.corriente[posicion]++;
				line_lcd4(dat1.corriente,1);
			}
		}
		if(stateB == 0 && menu1 == 1)
		{
			vTaskDelay(100/portTICK_RATE_MS);
			if(dat1.corriente[posicion] < 48) {dat1.corriente[posicion] = 48;}
			if(dat1.corriente[posicion] > 48)
			{
				dat1.corriente[posicion]--;
				line_lcd4(dat1.corriente,1);
			}
		}
		xQueueOverwrite(xQueue, &dat1);			//Envio los datos
	}
}
static void Principal2(void *pvParameters){
	unsigned char Datos_Rx [] = {0,0};
	float valor;
	int aa;
	struct dat dat1;
	while(1)
		{
		Chip_I2C_MasterSend (I2C1, SLAVE_ADDRESS, 0x01, 1);
		Chip_I2C_MasterRead (I2C1, 0x40, Datos_Rx, 2);
		valor = (Datos_Rx[1] << 8) + Datos_Rx[0];
		xQueuePeek(xQueue, &dat1, portMAX_DELAY);
		aa = dat1.corriente;
		}
	}

int main (void)
{
	SystemCoreClockUpdate();
	Board_Init();
	initlcd();	  //inicio el lcd
	initencoder();
	initi2c();

	xTaskCreate(Principal,"Principal",1024,0,1,0);      //Creacion de tarea Principal
	xTaskCreate(Principal2,"Principal2",1024,0,1,0);	//Creacion de tarea Principal2

    xQueue = xQueueCreate(1, sizeof(struct dat));		//Creacion de la cola, cant de elementos=1, tamaño de estrutura dat

	vTaskStartScheduler();								//Inicializacion del scheduler

	while(1)
	{

	}
}

/* INITLCD
 * Funcion para inicializar el lcd, asignando los pines necesarios y utlizando las funciones
 * de las bibliotecas del lcd.
 */

void initencoder(void){
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,2,encoderSW);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,0,encoderA);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,0,encoderB);
}

void initlcd(void){
	int j;
	//Seteo los pines como salida
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,puerto_lcd_R,RS);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,puerto_lcd_R,RW);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,puerto_lcd_R,E1);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,puerto_lcd_D,DB4);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,puerto_lcd_D,DB5);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,puerto_lcd_D,DB6);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,puerto_lcd_D,DB7);

	//Inicio los pines en cero
	Chip_GPIO_SetPinState(LPC_GPIO,puerto_lcd_R,RS,0);
	Chip_GPIO_SetPinState(LPC_GPIO,puerto_lcd_R,RW,0);
	Chip_GPIO_SetPinState(LPC_GPIO,puerto_lcd_R,E1,0);
	Chip_GPIO_SetPinState(LPC_GPIO,puerto_lcd_D,DB4,0);
	Chip_GPIO_SetPinState(LPC_GPIO,puerto_lcd_D,DB5,0);
	Chip_GPIO_SetPinState(LPC_GPIO,puerto_lcd_D,DB6,0);
	Chip_GPIO_SetPinState(LPC_GPIO,puerto_lcd_D,DB7,0);

	//Habilito las pull up interna
	Chip_IOCON_PinMux(LPC_IOCON,puerto_lcd_R,RS,MD_PUP,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,puerto_lcd_R,RW,MD_PUP,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,puerto_lcd_R,E1,MD_PUP,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,puerto_lcd_D,DB4,MD_PUP,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,puerto_lcd_D,DB5,MD_PUP,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,puerto_lcd_D,DB6,MD_PUP,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,puerto_lcd_D,DB7,MD_PUP,IOCON_FUNC0);

    ClearDisplay4();   //Limpio el display
    lcd_char_init4();  //Inicializo el lcd en 4 bits

 	line_lcd4("ELECTRONICLOAD",1);	//Escritura inicial
    line_lcd4(" IVAN SANTARI",2);	//Escritura inicial
}

void I2C1_IRQHandler(void)
{
	Chip_I2C_MasterStateHandler(I2C1);
}

void initi2c(void){
	/* Configuración de los pines. La EEPROM está conectada al puerto I2C1. */
	Chip_IOCON_PinMux(LPC_IOCON, SDA1_PORT, SDA1_PIN, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_PinMux(LPC_IOCON, SCL1_PORT, SCL1_PIN, IOCON_MODE_INACT, IOCON_FUNC3);

	/* Importante habilitar OPEN DRAIN */
	Chip_IOCON_EnableOD(LPC_IOCON, SDA1_PORT, SDA1_PIN);
	Chip_IOCON_EnableOD(LPC_IOCON, SCL1_PORT, SCL1_PIN);

	/* Habilita el módulo */
	Chip_I2C_Init(I2C1);

	/* Establece el clock */
	Chip_I2C_SetClockRate(I2C1, 100000);

	/* Handler de I2C Modo Master */
	Chip_I2C_SetMasterEventHandler(I2C1, Chip_I2C_EventHandler);

	/* Habilita la interrupción */
	NVIC_EnableIRQ(I2C1_IRQn);
}

